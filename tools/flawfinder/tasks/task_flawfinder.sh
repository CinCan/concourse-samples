#!/bin/sh
samples="input/samples/sources/*"
output_path="output/results"
toolscript="input/tools/flawfinder/python-tools/flawfinder.py"

# Crash early
set -e

git clone input output

if [ ! -d "$output_path" ]; then
    mkdir $output_path
fi
mkdir extracted

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

ls $samples

for filename in $samples
do
    unzip $filename -d extracted -P infected
done

ls extracted

flawfinder extracted > $output_path/output.txt

rm -rf extracted

cd output_path
ls results
git add results/*
git commit -m "dumped processes"
