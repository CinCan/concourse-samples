from util import execute_plugin, execute_dump

def dll(sample, profile, offset=None, pid=None, output_path=None):

    print "Running dlllist and dlldump..."
    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    output["dlllist"] = execute_plugin(params, ["dlllist"])

    if output_path:
        params += ["-D", output_path]
        execute_dump(params)

    return output