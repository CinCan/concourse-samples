from util import execute_dump

def dumpcerts(sample, profile, offset=None, pid=None, output_path=None):

    print "Running dumpcerts..."
    params = ["-f", sample,
              "--profile=" + profile,
              "certdump"
             ]
    output = {}

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    if output_path:
        params += ["-D", output_path]
        execute_dump(params)

    return output