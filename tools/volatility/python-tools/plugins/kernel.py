from util import execute_plugin

def kernel(sample, profile):

    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    output["dlllist"] = execute_plugin(params, ["dlllist"])

    if output_path:
        params += ["-D", output_path]
        execute_dump(params)

    return output