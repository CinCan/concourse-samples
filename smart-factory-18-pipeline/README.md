# Smart-Factory 2018 (20.-22.11.2018)

Presentation material and configuration files.

Link to the event: http://smart-factory.fi/

---

Usage:

1. Build the Concourse/Postgres/Gitlab environment using docker-compose.yml (see the pilot directory)
2. Ensure that you have created a local Gitlab user with the SSH public key
3. Add repository to your local Gitlab repository
4. Create directory "samples" and add one sample to it
5. Fill the `config/pipeline.yml` and `config/credentials.yml` files correctly (see the comments in the beginning of the files)